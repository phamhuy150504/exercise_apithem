// Lession 1
const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

;(function () {
    const max = 100;
    let content = ``;
    for (let init = 1; init <= max; init++) {
        content += `${init}   `;
        if (init % 10 === 0) {
            content += `<br />`;
        }
    }
    return $('#v-pills-lesson1').innerHTML = `<h2>
    In một bảng số từ 1-100 thỏa mãn điều kiện: (gợi ý dùng 2 vòng for lồng
    nhau, để ý chỗ bước nhảy)
    </h2>
    ${content}`;
}());

// lesson 2
let arrInputLesson2 = [];
const btn_lesson2 =  $('#btn__addInteger-lesson2')
btn_lesson2.onclick = () => {;
    const inputInteger = $('#input__lesson2')
        if(inputInteger.value !== '') {
            arrInputLesson2.push(inputInteger.value)
        }
    $('#showArr__lesson2').innerHTML = arrInputLesson2.join();
};

const btn_findInteger = $('#btn__lesson2-findInteger');
btn_findInteger.onclick = () => { 
    const arrInteger = arrInputLesson2.filter(num => {
        if(num < 2) {
            return false;
        } else if (num == 2){
            return true;
        } else {
            for (var i = 2; i < num; i++) {
                if (num % i === 0){
                    return false
                }
            }
            return true;
        }
    })
   $('#showInteger').innerHTML =  arrInteger.join();
};

// lesson 3 
const lesson3 = () => {
    const input_lesson3 = $('#input__lesson3').value*1;
    let sum = 0;
    if(input_lesson3 === '') {
        return;
    } else {
        for (let i = 2; i <= input_lesson3; i++) { 
            sum += i
        }
    }
    $('#result__lesson3').innerHTML = sum + 2*input_lesson3;
}

// lesson 4
const lesson4 = () => { 
    let arrLesson4 = [];
    const input_lesson4 = $('#input__lesson4').value*1;
    
    for (let i = 1; i <= input_lesson4; i++) { 
        if (input_lesson4 % i === 0) {
            arrLesson4.push(i)
        }
    }
    
    $('#result__lesson4').innerHTML = `Ước của ${input_lesson4}: ${arrLesson4.join()}`
};

// lesson 5
const lesson5 = () => {
    const input_lesson5 = $('#input__lesson5').value*1;
    if (input_lesson5 === '') {
        return;
    } else {
        $('#result__lesson5').innerHTML = input_lesson5.toString().split('').reverse().join('');
    }
};

// lesson 6
const lesson6 = () => { 
    const input__lesson6 = $('#input__lesson6').value*1;
    let arrLesson6 = [];
    let count = 0;
    for (let i  = 0; i <= input__lesson6; i++) { 
        if (count + i < input__lesson6) { 
            count += i;
            arrLesson6.push(i);
        }
    }
    console.log(arrLesson6[-1]);
    $('#result__lesson6').innerHTML = `X = ${arrLesson6[arrLesson6.length-1]}`;
};

// lesson 7
const lesson7 = () => { 
    const input__lesson7 = $('#input__lesson7').value*1;
    let contentHTML = ``;
    for (let i  = 1; i <= 10; i++) { 
        contentHTML += `${input__lesson7} * ${i} = ${input__lesson7 * i} </br>`;
    }
    $('#result__lesson7').innerHTML = contentHTML;
};

// lesson 8 
const lesson8 = () => { 
    const cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S",
    "AS", "7H", "9K", "10D"]
    let count = 0;
    let players = [[], [], [], []]
    cards.forEach(arr => {
        if (count > 3) {
            count = 0;
        } 
        players[count].push(arr);
        count++;
    })
    $('#player1').innerHTML = `Players 1: ${players[0]}`;
    $('#player2').innerHTML = `Players 1: ${players[1]}`;;
    $('#player3').innerHTML = `Players 1: ${players[2]}`;;
    $('#player4').innerHTML = `Players 1: ${players[3]}`;;
};

// lesson 9 
const lesson9 = () => { 
    const foot = $('#input__lesson9-foot').value*1;
    const animal = $('#input__lesson9-animal').value*1;
    const footDog = 4;
    const footChicken = 2;

    if (foot > 0 && animal > 0 && foot > animal) {
        const x = (foot - (animal * 4)) / (-2)
        const y = animal - x
       $('#result__lesson9').innerHTML = `Tổng Số Con Gà : ${x}
                                         Tổng Số Con Cho : ${y}`
    }


};

// lesson 10 
const lesson10 = () => { 
    const input__hour = $('#input__lesson10-hour').value*1;
    const input__minute = $('#input__lesson10-minute').value*1;
    
    if (input__hour === '' ||   input__minute === '' || input__minute > 60 || input__minute < 0 || input__hour > 12 || input__hour < 0) {
        alert('Xin mời nhập lại')
    } else {
        let gocHour = Math.abs(6 * input__minute - 0.5*(input__hour * 60 + input__minute))
        $('#result__lesson10').innerHTML = `Góc lệnh giữa kim giờ và kim phút là: ${gocHour} độ`
    }
};